from waitress import serve

from app import create_app
from config import Config

if __name__ == "__main__":
    config = Config()
    print("Starting server...")
    application = create_app()
    print("Application Created...")
    print("Using Localhost: {}".format(config.USE_LOCAL))
    serve(application, host='0.0.0.0',
          port=int(config.APP_PORT),
          threads=8
          )
