# Create a Flask app
from flask import Flask, Blueprint

app = Flask(__name__)

bp = Blueprint('main', __name__)

from app.routes import routes
