import json
import logging
import os

from flask import Response, current_app, render_template, request

from config import Config
from . import bp

log = logging.getLogger(current_app.name)


@bp.route("/health")
def health():
    service_status = "UP"
    status_code = 200

    data = {"status": "{}".format(service_status), "v1": True}
    response = Response(
        response=json.dumps(data),
        status=status_code,
        mimetype="application/json",
        content_type="application/json",
    )

    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Cache-Control", "no-store")

    return response


@bp.route("/", methods=["GET"])
def index():
    log.debug("HOME PAGE")
    use_localhost = Config().USE_LOCAL
    log.info("USE LOCALHOST: {}".format(use_localhost))

    return render_template("index.html", IS_LOCALHOST=use_localhost)


@bp.route("/ping", methods=["GET"])
def ping():
    log.debug("PONG PAGE")
    status_code = 200

    message = request.args.get("message")

    data = {"pong": "{}".format(message)}
    response = Response(
        response=json.dumps(data),
        status=status_code,
        mimetype="application/json",
        content_type="application/json",
    )

    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Cache-Control", "no-store")

    return response
