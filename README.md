# Overview

This is a Python Flask application that send a randomly generated string to another instance of itself acting as a 
Ping -> Pong. 

One of the practical applications of this is to use the application to demonstrate a modernization story for VMs and Pods.

## Ping

The Ping side will generate a random string using JavaScript, then `get` (via `fetch()`) using query parameter `message` 
to the Pong server. When response is returned, the JavaScript will update the text area with the returned Pong response

## Pong

In a second instance, run the same application. The application will listen to `/pong` and a query parameter `message`. 
The route will turn around and respond with the `message` in a JSON body.

> NOTE: If running the Ping and Pong on the same server, set different PORT values (see `.env` or set an ENV variable)

## Script
1. Show no VM
2. Start VM (using Git Commit)
3. Log into the VM with RDP (remina)
4. Start both Python apps (Ping and Pong)
5. Show brower Ping -> Pong works 
6. Stop Pong
7. Deploy Pong on K8s (Git Commit)
8. Restart the Python Ping (maybe I can do some JS magic to avoid restarting app)
9. See that the VM is using the K8s Pong (Ping->Pong still works)
10. done!

## Example K8s Manifest

Deploying to K8s, use the `Dockerfile` to generate and push the artifact to a repo where the K8s cluster can pull (this guide will not cover 
authentication for k8s and registries).

Apply this manifest to your k8s and deploy the latest Ping-Pong server

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cors-ping-pong-deployment
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: cors-ping-pong
  template:
    metadata:
      labels:
        app: cors-ping-pong
    spec:
      containers:
      - image: registry.gitlab.com/mike-ensor/cors-ping-pong/cors-ping-pong:v0.0.3
        imagePullPolicy: IfNotPresent
        envs:
        - name: APP_PORT
          value: "5001"
        livenessProbe:
          httpGet:
            path: /health
            port: 5001
          initialDelaySeconds: 3
          periodSeconds: 5
        name: cors-cors-ping-pong
        env:
        - name: APP_PORT
          value: "5001"
        readinessProbe:
          httpGet:
            path: /health
            port: 5001
          initialDelaySeconds: 5
          periodSeconds: 5
        securityContext:
          allowPrivilegeEscalation: false
---
apiVersion: v1
kind: Service
metadata:
  name: pong-server-svc
  namespace: default
spec:
  ports:
  - name: application
    port: 8080
    protocol: TCP
    targetPort: 5001
  selector:
    app: cors-ping-pong
  type: LoadBalancer
```