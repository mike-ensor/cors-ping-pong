import os

from dotenv import dotenv_values


class Config:

    def __init__(self):
        self.config = {
            **dotenv_values(".env"),  # load shared development variables
            **os.environ,  # override loaded values with environment variables
        }
        self.USE_LOCAL = self.config.get("USE_LOCAL")
        self.APP_PORT = int(self.config.get("APP_PORT", "5000"))
        self.LOG_COLOR = True
        self.HTTP_LOG_LEVEL = self.config.get("HTTP_LOG_LEVEL", "INFO")
        self.APP_LOG_LEVEL = self.config.get("APP_LOG_LEVEL", "INFO")
